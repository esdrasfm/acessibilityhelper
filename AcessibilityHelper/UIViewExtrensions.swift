//
//  UIViewExtrensions.swift
//  Utils
//
//  Created by Esdras Firmino Martins on 24/09/18.
//  Copyright © 2018 Serasa Experian. All rights reserved.
//

import UIKit
import AudioToolbox

public extension UIView {
    public func shake() {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        animation.duration = 0.6
        animation.values = [-10.0, 10.0, -10.0, 10.0, -5.0, 5.0, -2.0, 2.0, 0.0 ]
        layer.add(animation, forKey: "shake")
        
        let generator = UIImpactFeedbackGenerator(style: .heavy)
        generator.impactOccurred()
        
        //        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
    }
    
    public func translate(x: CGFloat = 0, y: CGFloat = 0, completion: (()->Void)? = nil) {
        UIView.animate(withDuration: 0.6, animations: {
            self.transform = self.transform.translatedBy(x: x, y: y)
        }) { (completed) in
            completion?()
        }
    }
    
    final class Lifted<T> {
        let value: T
        init(_ x: T) {
            value = x
        }
    }
    
    private func lift<T>(_ x: T) -> Lifted<T>  {
        return Lifted(x)
    }
    
    func associated<T>(to base: AnyObject,
                       key: UnsafePointer<UInt8>,
                       policy: objc_AssociationPolicy = .OBJC_ASSOCIATION_RETAIN,
                       initialiser: () -> T) -> T {
        if let v = objc_getAssociatedObject(base, key) as? T {
            return v
        }
        
        if let v = objc_getAssociatedObject(base, key) as? Lifted<T> {
            return v.value
        }
        
        let lifted = Lifted(initialiser())
        objc_setAssociatedObject(base, key, lifted, policy)
        return lifted.value
    }
    
    func associate<T>(to base: AnyObject, key: UnsafePointer<UInt8>, value: T, policy: objc_AssociationPolicy = .OBJC_ASSOCIATION_RETAIN) {
        
        let v: AnyObject = value as AnyObject
        objc_setAssociatedObject(base, key, v, policy)
    }
    
    struct Keys {
        static fileprivate var accOrder: UInt8 = 0
        static fileprivate var analyticsLabel: UInt8 = 0
        static fileprivate var analyticsCategory: UInt8 = 0
    }
    
    @IBInspectable public var accessibilityOrder: Int{
        
        get {
            return associated(to: self, key: &Keys.accOrder) {
                return 999
            }
        }
        set {
            associate(to: self, key: &Keys.accOrder, value: newValue)
        }
    }
    
    public var allSubviews: [UIView] {
        return self.subviews.reduce([UIView]()) { $0 + [$1] + $1.allSubviews }
    }

    public func loadNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nibName = type(of: self).description().components(separatedBy: ".").last!
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as! UIView
    }
    
    @IBInspectable public var analyticsLabel: String {
        get {
            return associated(to: self, key: &Keys.analyticsLabel) {
                return ""
            }
        }
        set {
            associate(to: self, key: &Keys.analyticsLabel, value: newValue)
        }
    }
    
    @IBInspectable public var analyticsCategory: String {
        get {
            return associated(to: self, key: &Keys.analyticsCategory) {
                return ""
            }
        }
        set {
            associate(to: self, key: &Keys.analyticsCategory, value: newValue)
        }
    }
    
    public func startShimmering() {
    
        let lightColor = UIColor(white: 0.0, alpha: 0.3).cgColor
        let darkColor = UIColor.black.cgColor
        
        let colorArray: [CGColor] = [darkColor, lightColor, darkColor]
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = colorArray
        gradientLayer.frame = CGRect(x: -self.bounds.size.width, y: 0, width: 3 * self.bounds.size.width, height: self.bounds.height)
        
        
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.8)
        gradientLayer.endPoint   = CGPoint(x: 0.7, y: 1.0)
        gradientLayer.locations  = [0.4, 0.45, 0.5]
        self.layer.mask = gradientLayer
        
        let animation = CABasicAnimation(keyPath: "locations")
        animation.fromValue = [0.0, 0.1, 0.2];
        animation.toValue   = [0.8, 0.9, 1.0];
        animation.duration = 1.5;
        animation.fillMode = kCAFillModeForwards
        
        let animGroup = CAAnimationGroup()
        
        animGroup.animations = [animation]
        animGroup.duration = 2.5
        animGroup.repeatCount = HUGE
        gradientLayer.add(animGroup, forKey: "shimmer")
    }
}
