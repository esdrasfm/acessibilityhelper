//
//  AccesibilityViewController.swift
//  TesteAcessibilidade
//
//  Created by Deh on 06/10/18.
//  Copyright © 2018 Deh. All rights reserved.
//

import UIKit

public class AccessibilityViewController: UIViewController {

    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        setupAccessibilityElements()
    }
    
    func setupAccessibilityElements() {
        var accElemens = [Any]()
        
        var readyViews: Set<UIView>! = []
        
        var i = 0

        let orderedViews = self.view.allSubviews.sorted {
            return $0.accessibilityOrder < $1.accessibilityOrder
        }
        while (i < orderedViews.count) {
            var j = i+1
            let outerView = orderedViews[i]
            if(outerView.accessibilityOrder == 999 || readyViews.contains(outerView)) {
                i += 1
                continue
            }
            
            while (j < orderedViews.count) {
                
                let innerView = orderedViews[j]
                if (!innerView.isAccessibilityElement || readyViews.contains(innerView) ) {
                    j+=1
                    continue
                    
                }
                if ( outerView.accessibilityOrder == innerView.accessibilityOrder) {
                    readyViews.insert(innerView)
                    outerView.accessibilityLabel = outerView.accessibilityLabel! + ", " + innerView.accessibilityLabel!
                    
                    outerView.accessibilityFrame = outerView.accessibilityFrame.union(innerView.accessibilityFrame)
                }
                
                j += 1
            }
            
            i += 1

            accElemens.append(outerView)
        }
        
        self.view.accessibilityElements = accElemens
    }
}
